# WinTK2Unix

Run Windows J2ME SDKs on native Unix IDEs

![Devices reconized on Netbeans](./pics/screenshot1.png)
![Emulator on Netbeans](./pics/screenshot2.png)
![Devices reconized on Eclipse](./pics/screenshot3.png)

## Instructions

 1. Download repository
 2. Extract files from "src" in WTK's root
        
        For example: src/* to ~/.wine/drive_c/Samsung_SDK_122/

 3. Modify variables values in $WTK_ROOT/prefix

        Essential: WINEPREFIX, WINDOWS_WTK_PATH and UNIX_WTK_PATH


##  ¿How does it works?

When using native IDEs, it's expected that your tools talks in Unix language, however that doesn't occurs when using Windows tools through Wine.

When your IDE tries to run your compiled code, it sends your tool the full path in Unix format, while it should've sent in Windows format. Or when it asks for libraries path, devices etc, it returns in Windows path format, while it should have responded in Unix format.

Using native IDEs is much preferable as running them through Wine has poorer perfomance.

This script solves that translating Windows paths to Unix paths and vice-versa, so they work in native IDEs.

## Tested IDEs:

  * Netbeans 8.2 (Recommended)
  * Eclipse with MTJ plugin

## Tested J2ME SDKs:

  * Samsung J2ME SDK
  * LG SDK 1.5